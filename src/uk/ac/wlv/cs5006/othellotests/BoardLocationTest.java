package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.BoardLocation;

public class BoardLocationTest {
	@Test
	public void testDefaultConstructor(){
		BoardLocation boardLocation = new BoardLocation();
		assertTrue(boardLocation.isInvalid());
	}
	
	@Test
	public void testParameterisedConstructor(){
		BoardLocation boardLocation = new BoardLocation(4, 4);
		assertFalse(boardLocation.isInvalid());
		assertEquals(4, boardLocation.getRow());
		
		boardLocation = new BoardLocation(6, 8);
		assertEquals(8, boardLocation.getCol());
		
		boardLocation = new BoardLocation(6, -1);
		assertTrue(boardLocation.isInvalid());
	}
	
	@Test
	public void testSetInvalid(){
		BoardLocation boardLocation = new BoardLocation();
		boardLocation.setInvalid();
		assertTrue(boardLocation.isInvalid());
		
		boardLocation = new BoardLocation(4, 4);
		boardLocation.setInvalid();
		assertTrue(boardLocation.isInvalid());
	}
	
	@Test
	public void testSetterGetter(){
		BoardLocation boardLocation = new BoardLocation();
		boardLocation.setCol(4);
		assertEquals(4, boardLocation.getCol());
		
		boardLocation.setRow(4);
		assertEquals(4, boardLocation.getRow());
	}
}
