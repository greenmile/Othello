package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.BoardCell;
import uk.ac.wlv.cs5006.othello.Item;

public class BoardCellTest {
	private static final String WHITE = "WHITE";
	private static final String BLACK = "BLACK";
	
	@Test
	public void testConstructor(){
		Item token = new Item(WHITE);
		BoardCell boardCell = new BoardCell(4, 4, token);
		
		assertEquals(token, boardCell.getItem());
		assertNotNull(boardCell.getItem().getValue());
		assertEquals(WHITE, boardCell.getItem().getValue());
		assertEquals(4, boardCell.getLocation().getRow());
		assertEquals(4, boardCell.getLocation().getCol());
		assertFalse(boardCell.getLocation().isInvalid());
		
		
		
		token = new Item(BLACK);
		boardCell = new BoardCell(7, 8, token);
		
		assertEquals(token, boardCell.getItem());
		assertNotNull(boardCell.getItem().getValue());
		assertEquals(BLACK, boardCell.getItem().getValue());
		assertEquals(7, boardCell.getLocation().getRow());
		assertEquals(8, boardCell.getLocation().getCol());
		assertFalse(boardCell.getLocation().isInvalid());
		
		
		
		token = new Item(BLACK);
		boardCell = new BoardCell(7, -1, token);
		
		assertEquals(BLACK, boardCell.getItem().getValue());
		assertTrue(boardCell.getLocation().isInvalid());
	}
	
	@Test
	public void testSetterGetter(){
		BoardCell boardCell = new BoardCell(-1, -1, null);
		assertNull(boardCell.getItem());
		
		Item token = new Item(WHITE);
		boardCell.setItem(token);
		assertNotNull(boardCell.getItem());
		assertTrue(boardCell.getItem().equals(token));
		
		boardCell.setValue(BLACK);
		assertEquals(BLACK, boardCell.getItem().getValue());
	}
}
