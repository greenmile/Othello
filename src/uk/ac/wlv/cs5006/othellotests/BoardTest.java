package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Board;
import uk.ac.wlv.cs5006.othello.Item;

public class BoardTest {
	
	public static int rows = 8;
	public static int cols = 8;
	public static String EMPTY = "EMPTY";
	private static final String WHITE = "WHITE";
	private static final String BLACK = "BLACK";
	
	@Test
	public void testConstructor(){
		Board board = new Board(rows, cols);
		assertEquals(rows, board.getNumRows());
		assertEquals(cols, board.getNumColumns());
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				Item item = board.getItem(i, j);
				assertNotNull(item);
				assertEquals(EMPTY, item.getValue());;
			}
		}
	}
	
	@Test
	public void testSetterGetter(){
		Board board = new Board(0, 0);
		assertNull(board.getItem(0, 0));		
		assertFalse(board.setItemValue(rows, cols, WHITE));
		
		board = new Board(rows, cols);
		assertEquals(rows, board.getNumRows());
		assertEquals(cols, board.getNumColumns());
		assertNotNull(board.getItem(0, 0));
		assertTrue(board.setItemValue(0, 0, WHITE));
	}
	
	@Test
	public void testClear(){
		Board board = new Board(rows, cols);
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				board.setItemValue(i, j, WHITE);
			}
		}
		
		board.clear();
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				Item item = board.getItem(i, j);
				assertTrue(item.getValue().equalsIgnoreCase(EMPTY));
			}
		}
	}
	
	@Test
	public void testIsOnBoard(){
		Board board = new Board(rows, cols);
		assertFalse(board.isOnBoard(rows, cols));
		assertTrue(board.isOnBoard(1, 5));
	}
	
	@Test
	public void testBoardIsFull(){
		Board board = new Board(rows, cols);
		assertFalse(board.boardIsFull());
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				board.setItemValue(i, j, WHITE);
			}
		}
		assertTrue(board.boardIsFull());
	}
	
	@Test
	public void testBoardHasSingleValue(){
		Board board = new Board(rows, cols);
		//TODO maybe bug
		assertTrue(board.boardHasSingleValue());
		
		//comments: if EMPTY treat as a COLOR than EMPTY and WHITE should different
		board.setItemValue(1, 0, WHITE);
		assertTrue(board.boardHasSingleValue());
		
		board.setItemValue(1, 1, BLACK);
		assertFalse(board.boardHasSingleValue());
	}
}
