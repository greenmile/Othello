package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Item;

public class ItemTest {
	
	private static final String WHITE = "WHITE";
	private static final String BLACK = "BLACK";
	
	@Test
	public void testConstructor(){
		Item item = new Item(WHITE);
		assertEquals(WHITE, item.getValue());
		
		item = new Item(BLACK);
		assertEquals(BLACK, item.getValue());
	}
	
	@Test
	public void testSetterGetter(){
		Item item = new Item(null);
		assertNull(item.getValue());
		
		item.setValue(WHITE);
		assertNotNull(item.getValue());
		assertEquals(WHITE, item.getValue());		
	}
	
	@Test
	public void testEquals(){
		Item item1 = new Item(WHITE);
		Item item2 = new Item(WHITE);
		assertTrue(item1.equals(item2));
		
		item1 = new Item(WHITE);
		item2 = new Item(BLACK);
		assertFalse(item1.equals(item2));		
	}	
}
