package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Controller;
import uk.ac.wlv.cs5006.othello.Item;
import uk.ac.wlv.cs5006.othello.OthelloModel;

public class ControllerTest {
	public static int rows = 8;
	public static int cols = 8;
	public static String EMPTY = "EMPTY";
	private static final String WHITE = "WHITE";
	private static final String BLACK = "BLACK";
	private static final String NONE = "NONE";
	
	@Test
	public void testAttemptPlay(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		Controller controller = new Controller(othelloModel);
		
		assertFalse(controller.attemptPlay(0, 0, null));
		assertFalse(controller.attemptPlay(9, 7, new Item(BLACK)));
		assertFalse(controller.attemptPlay(3, 3, new Item(WHITE)));
		
		assertFalse(controller.attemptPlay(0, 4, new Item(BLACK)));
		assertTrue(controller.attemptPlay(2, 4, new Item(BLACK)));
		assertFalse(controller.attemptPlay(4, 5, new Item(BLACK)));
	}
	
	@Test
	public void testGetOpponent(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		Controller controller = new Controller(othelloModel);
		
		assertNotNull(controller.getOpponent(""));
		assertNotNull(controller.getOpponent(null));
		assertEquals(WHITE, controller.getOpponent(BLACK));
		assertEquals(BLACK, controller.getOpponent(WHITE));
	}
	
	@Test
	public void testGetWinner(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		Controller controller = new Controller(othelloModel);
		
		assertEquals(NONE, controller.getWinner());
		controller.attemptPlay(2, 4, new Item(BLACK));
		assertEquals(BLACK, controller.getWinner());
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<rows; j++){
				Item item = controller.getItem(i, j);
				item.setValue(WHITE);
			}
		}
		assertEquals(WHITE, controller.getWinner());
	}
}
