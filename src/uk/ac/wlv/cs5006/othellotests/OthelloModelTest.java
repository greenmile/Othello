package uk.ac.wlv.cs5006.othellotests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.othello.Item;
import uk.ac.wlv.cs5006.othello.OthelloModel;

public class OthelloModelTest {
	public static int rows = 8;
	public static int cols = 8;
	public static String EMPTY = "EMPTY";
	private static final String WHITE = "WHITE";
	private static final String BLACK = "BLACK";
	private static final String NONE = "NONE";
	
	@Test
	public void testSetInitialState(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		
		assertEquals(BLACK, othelloModel.getItem(3, 3).getValue());
		assertEquals(WHITE, othelloModel.getItem(3, 4).getValue());
		assertEquals(WHITE, othelloModel.getItem(4, 3).getValue());
		assertEquals(BLACK, othelloModel.getItem(4, 4).getValue());
	}
	
	
	@Test
	public void testSetterGetter(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		assertEquals(BLACK, othelloModel.getItem(3, 3).getValue());
		
		assertEquals(rows, othelloModel.getNumRows());
		assertEquals(cols, othelloModel.getNumColumns());
		
		assertNotNull("Should not null", othelloModel.getOpponent(null));
		assertNotNull(othelloModel.getOpponent(""));
		
		assertEquals(EMPTY, othelloModel.getOpponent(EMPTY));
		assertEquals(WHITE, othelloModel.getOpponent(BLACK));
		assertEquals(BLACK, othelloModel.getOpponent(WHITE));		
	}
	
	@Test
	public void testAttemptPlay(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		
		assertFalse(othelloModel.attemptPlay(0, 0, null));
		assertFalse(othelloModel.attemptPlay(9, 7, new Item(BLACK)));
		assertFalse(othelloModel.attemptPlay(3, 3, new Item(WHITE)));
		
		assertFalse(othelloModel.attemptPlay(0, 4, new Item(BLACK)));
		assertTrue(othelloModel.attemptPlay(2, 4, new Item(BLACK)));
		assertFalse(othelloModel.attemptPlay(4, 5, new Item(BLACK)));
	}
	
	@Test
	public void testBoardIsFull(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<rows; j++){
				Item item = othelloModel.getItem(i, j);
				item.setValue(BLACK);
			}
		}
		assertTrue(othelloModel.boardIsFull());
	}
	
	@Test
	public void testBoardHasSingleValue(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		assertFalse(othelloModel.boardHasSingleValue());
		othelloModel.setInitialState();
		assertFalse(othelloModel.boardHasSingleValue());
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<rows; j++){
				Item item = othelloModel.getItem(i, j);
				item.setValue(BLACK);
			}
		}
		assertTrue(othelloModel.boardHasSingleValue());
	}
	
	
	@Test
	public void testGetOpponent(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		assertNotNull(othelloModel.getOpponent(""));
		assertNotNull(othelloModel.getOpponent(null));
		assertEquals(WHITE, othelloModel.getOpponent(BLACK));
		assertEquals(BLACK, othelloModel.getOpponent(WHITE));
	}
	
	@Test
	public void testGetWinner(){
		OthelloModel othelloModel = new OthelloModel(rows, cols);
		othelloModel.setInitialState();
		assertEquals(NONE, othelloModel.getWinner());
		othelloModel.attemptPlay(2, 4, new Item(BLACK));
		assertEquals(BLACK, othelloModel.getWinner());
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<rows; j++){
				Item item = othelloModel.getItem(i, j);
				item.setValue(WHITE);
			}
		}
		assertEquals(WHITE, othelloModel.getWinner());
	}
	
}
