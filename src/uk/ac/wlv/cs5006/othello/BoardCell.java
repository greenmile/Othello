package uk.ac.wlv.cs5006.othello;

/**
 * This class represents a single cell on the Board. It has a location and a token value.
 * @author ianskenny
 * @since 14/15
 */
public class BoardCell
{
	/**
	 * The Item at this BoardCell.
	 */
	private Item token;

	/**
	 * The CellLocation of this BoardCell.
	 */
	private BoardLocation location;

	/**
	 * Constructor.
	 * @param row the row number.
	 * @param col the column number.
	 * @param token the Item value.
	 */
	public BoardCell(int row, int col, Item token)
	{
		this.token = token;
		location = new BoardLocation(row, col);
	}

	/**
	 * Sets the Item value.
	 * @param token the Item value.
	 */
	public void setItem(Item token)
	{
		this.token = token;
	}

	/**
	 * Set the value of the Item in this BoardCell.
	 * @param val the value of the Item.
	 */
	public void setValue(String val)
	{
		this.token.setValue(val);
	}

	/**
	 * Gets the Item value.
	 * @return the Item at this BoardCell.
	 */
	public Item getItem()
	{
		return token;
	}

	/**
	 * Get the BoardLocation for this BoardCell.
	 * @return the BoardLocation for this BoardCell.
	 */
	public BoardLocation getLocation()
	{
		return location;
	}
}
