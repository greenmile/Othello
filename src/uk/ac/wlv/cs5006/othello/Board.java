package uk.ac.wlv.cs5006.othello;

/**
 * This class represents the game Board.
 * @author ianskenny
 * @since 14/15
 */

public class Board
{
	/**
	 * The number of rows in the Board.
	 */
	private int rows;

	/**
	 * The number of columns in the Board.
	 */
	private int cols;

	/**
	 * The Board as a 2d array of BoardCell objects.
	 */
	private BoardCell[][] cells;


	/**
	 * Constructor
	 * @param nrows the number of rows on this Board.
	 * @param ncols the number of columns on this Board.
	 */
  	public Board(int nrows, int ncols)
	{
  		rows = nrows;
  		cols = ncols;
  		cells = new BoardCell[rows][cols];
  		clear();
	}

  	/**
  	 * Gets the number of rows on this Board.
  	 * @return the number of rows on this Board.
  	 */
  	public int getNumRows()
  	{
  		return rows;
  	}

  	/**
  	 * Gets the number of columns on this Board.
  	 * @return the number of columns on this Board.
  	 */
  	public int getNumColumns()
  	{
  		return cols;
  	}


	/**
	 * Clears the Board and sets all tokens to EMPTY
	 */
	public void clear()
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				cells[i][j] = new BoardCell(i, j, new Item("EMPTY"));
			}
		}
	}

	/**
	 * Sets the given cell to the Item value.
	 * @param row the row number.
	 * @param col the column number.
	 * @param value the Item value.
	 */
	public boolean setItemValue(int row, int col, String value)
	{
		 if (isOnBoard(row, col))
		 {
			 cells[row][col].setValue(value);
			 return true;
		 }

		 return false;
	}

	/**
	 * Gets the Item at the given cell.
	 * @param row the row number.
	 * @param col the column number.
	 * @return the Item at the given cell.
	 */
	public Item getItem(int row, int col)
	{
		 if (isOnBoard(row, col))
		 {
			 return cells[row][col].getItem();
		 }

		 return null;
	}

	/**
	 * Tests if the cell represented by (row, col) is on the Board.
	 * @param row the row number.
	 * @param col the column number.
	 * @return true if the cell is on the Board, false otherwise.
	 */
	public boolean isOnBoard(int row, int col)
	{
		 if (row >= 0 && row < rows && col >= 0 && col < cols)
		 {
			 return true;
		 }

		 else return false;
	}

	/**
	 * Checks if the Board is full.
	 * @return true if the Board is full, false otherwise (i.e. one or more EMPTY locations).
	 */
	public boolean boardIsFull()
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (getItem(i, j).getValue().equals("EMPTY"))
					return false;
			}
		}

		return true;
	}

	/**
	 * Checks if the Board contains only one colour.
	 * @return true if the Board contains a single colour, false otherwise.
	 */
	public boolean boardHasSingleValue()
	{
	    // find the first token
	    Item checkItem = null;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                if (!(getItem(i, j).getValue().equals("EMPTY")))
                    checkItem = getItem(i, j);
            }
        }

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (!getItem(i, j).getValue().equals("EMPTY") && (!getItem(i, j).getValue().equals(checkItem.getValue())))
					return false;
			}
		}

		return true;
	}
}
